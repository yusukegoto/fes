# Fes

Create Fes Playlists if specified spotify account does not have playlists.

## Setup

### Required

Install `direnv`.
At first you need to create `.envrc`.

```
cat <<ENVRC > .envrc
export SPOTIPY_CLIENT_ID=XXXXXXXXX
export SPOTIPY_CLIENT_SECRET=YYYYYYYYY
export SPOTIPY_REDIRECT_URI=https://s3-ap-northeast-1.amazonaws.com/private.yusuke-goto.info/redirect_handle.html
ENVRC
```

requirements `python 3.6.1`.

```
$ python -m vnev myenv
$ . myenv/bin/active
> pip install -r requirements.txt
```

### Optional
```
cat <<ENVRC >> .envrc
export SPOTIPY_PUBLIC=XXXXXXXXX
export SPOTIPY_MARKET=YYYYYYYYY
ENVRC
```

`SPOTIPY_PUBLIC`: Default playlist policy is private.
If you want to create a playlist with pubic policy, set environment variable `SPOTIPY_PUBLIC`.
Be careful this program doesn't check what `SPOTIPY_PUBLIC`'s value is set. Setting `SPOTIPY_PUBLIC` will publish with public policy.

`SPOTIPY_MARKET`: Default is `JP`. An ISO 3166-1 alpha-2 country code.

## Execute

Once You exec the command, Web brower will ask you to allow access your spotify info.
After allowing, you put redirected URL to command promt.

```
$ ./fes.py


            User authentication requires interaction with your
            web browser. Once you enter your credentials and
            give authorization, you will be redirected to
            a url.  Paste that url you were directed to to
            complete the authorization.


Opened https://accounts.spotify.com/authorize?client_id=XXXXXXXXX&response_type=code&redirect_uri=YYYYYYYYYY&scope=user-library-read in your browser


Enter the URL you were redirected to:
```

## Festivals

- fuji17
- summersonic2017
- idolfes2017
- bonnaroo2017
- lollapalooza2017
- afterhours2019

## Code Status

[![CircleCI](https://circleci.com/bb/yusukegoto/fes.svg?style=svg)](https://circleci.com/bb/yusukegoto/fes)
