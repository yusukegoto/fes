#!/usr/bin/env python

import os
import re
import sys

import requests
import spotipy
import spotipy.util as util
from bs4 import BeautifulSoup

festivals = {
    'fuji17': {
        'title': 'FUJI ROCK FESTIVAL 17',
        'url': 'http://www.fujirockfestival.com/artist/index.html',
        'selector': 'a.pop_detail',
        'pattern': r'</?a[^>]*>',
    },
    'summersonic2017': {
        'title': 'Summer Sonic 2017',
        'url': 'http://www.summersonic.com/2017/lineup/',
        'selector': '.listBox a dd',
        'pattern': r'(</?(dd|small)>|(（.+）))',
    },
    'sonicmania2017': {
        'title': 'Sonic Mania 2017',
        'url': 'http://www.summersonic.com/2017/lineup/mania.html',
        'selector': '.listBox a dd',
        'pattern': r'(</?(dd|small)>|(（.+）))',
    },
    'idolfes2017': {
        'title': 'TOKYO IDOL FESTIVAL 2017',
        'url': 'http://www.idolfes.com/2017/lineup/',
        'selector': '.unitBox img',
        'attr': 'alt',
    },
    'bonnaroo2017': {
        'title': 'Bonnaroo 17',
        'url': 'https://www.bonnaroo.com/lineup/interactive/',
        'selector': '.c-lineup__caption-text',
    },
    'lollapalooza2017': {
        'title': 'Lollapalooza 2017',
        'url': 'https://www.lollapalooza.com/lineup/interactive-lineup/',
        'selector': '.c-lineup__caption-text',
    },
    'clockenflap2017': {
        'title': 'Clockenflap 2017',
        'url': 'http://www.clockenflap.com/lineup',
        'file': 'clockenflap2017.csv',
    },
    'mutekjp2017': {
        'title': 'Mutek JP 2017',
        'url': 'http://mutek.jp/pages/mutek_jp',
        'file': 'mutekjp2017.csv',
    },
    'coachella2018': {
        'title': 'Coachella 2018',
        'url': 'http://mutek.jp/pages/mutek_jp',
        'file': 'coachella2018.csv',
    },
    'fuji2018': {
        'title': 'FUJI ROCK FESTIVAL 2018',
        'url': 'http://www.fujirockfestival.com/artist/index.html',
        'selector': 'a.pop_detail',
    },
    'summersonic2018': {
        'title': 'Summer Sonic 2018',
        'url': 'http://www.summersonic.com/2018/lineup/',
        'selector': '.listBox a dd',
        'pattern': r'(</?(dd|small)>|(（.+）))',
    },
    'sonicmania2018': {
        'title': 'Sonic Mania 2018',
        'url': 'http://www.summersonic.com/2018/lineup/mania.html',
        'selector': '.listBox a dd',
        'pattern': r'(</?(dd|small)>|(（.+）))',
    },
    'riot2018': {
        'title': 'Riot Fest 2018',
        'url': 'https://consequenceofsound.net/festival/riot-fest-chicago-2018/',
        'selector': '.artist',
    },
    'afterhours2019': {
        'title': 'After Hours Tokyo 2019',
        'url': 'https://www.afterhours.live/ja/',
        'file': 'afterhours2019.csv',
    },
}

PRIVATE_POLICIES = 'playlist-read-private playlist-modify-private'
PUBLIC_POLICES = 'playlist-modify-public'


class Spotify:
    def __init__(self):
        self.is_public = 'SPOTIPY_PUBLIC' in os.environ
        token = self._get_token()
        self.sp = spotipy.Spotify(auth=token)
        self.user_id = self.sp.me()['id']
        self.country = os.environ.get('SPOTIPY_MARKET', 'JP')

    def find_playlist(self, title, offset=0):
        results = self.sp.current_user_playlists(offset=offset)

        for item in results['items']:
            name = item['name']

            if title == name:
                return item

        if results['next']:
            next_offset = offset + 1
            return self.find_playlist(title, offset=next_offset)

    def delete_all_tracks(self, playlist_id):
        while True:
            result = self.sp.user_playlist_tracks(self.user_id, playlist_id)
            track_ids = [item['track']['id'] for item in result['items']]

            if track_ids:
                self.sp.user_playlist_remove_all_occurrences_of_tracks(
                    self.user_id, playlist_id, track_ids)

            if not result['next']:
                break

    def recreate_playlist(self, title):
        playlist = self.find_playlist(title)

        if not playlist:
            playlist = self.sp.user_playlist_create(
                self.user_id, title, public=self.is_public)
        self.delete_all_tracks(playlist['id'])

        return playlist

    def _get_token(self):
        scope = PUBLIC_POLICES if self.is_public else PRIVATE_POLICIES
        username = os.environ['SPOTIPY_USERNAME']
        token = util.prompt_for_user_token(username, scope)
        return token

    def get_artist(self, name):
        try:
            result = self.sp.search(q=name, type='artist')
            return result['artists']['items'][0]
        except IndexError:
            print('Not Found: {}'.format(name))

    def get_top_tracks(self, artist):
        res = self.sp.artist_top_tracks(artist['id'], country=self.country)
        for track in res['tracks']:
            yield track

    def add_top_tracks(self, playlist, artist_names):
        for artist_name in artist_names:
            artist = self.get_artist(artist_name)

            if not artist:
                continue

            track_ids = [track['id'] for track in self.get_top_tracks(artist)]
            track_ids = track_ids[0:5]
            print(artist['name'])

            if not track_ids:
                continue

            self.sp.user_playlist_add_tracks(
                self.user_id,
                playlist['id'],
                track_ids
            )


def main(festival):
    sp = Spotify()

    festival = festivals[festival]
    url = festival['url']
    title = festival['title']

    filename = festival.get('file')
    selector = festival.get('selector')
    pattern = festival.get('pattern')
    attr = festival.get('attr')

    artist_names = get_artist_names(
        url=url,
        selector=selector,
        pattern=pattern,
        attr=attr,
        filename=filename,
    )
    playlist = sp.recreate_playlist(title=title)

    sp.add_top_tracks(playlist, artist_names)

    print('\nSuccessfully created!!')


def get_artist_names(url, selector, pattern=None, attr=None, filename=None):
    """
    Need to set selector or filename. Cannot set both at the same time.
    If filename exists, its priority is higher than selector.
    """
    if filename is None:
        artist_ps = _get_soup(url).select(selector)

        artist_names = []
        for p in artist_ps:
            artist_name = extract_artist(
                p, pattern=pattern, attr=attr,
            ).strip(
            ).replace(
                'amp;', ''
            )

            if artist_name:
                artist_names.append(artist_name)
    else:
        filepath = 'csv/{}'.format(filename)
        with open(filepath) as f:
            artist_names = [_line.strip() for _line in f]

    return artist_names


def extract_artist(html_str, pattern=None, attr=None):
    html_str_ = str(html_str)

    if pattern:
        return re.sub(pattern, '', html_str_)
    elif attr:
        return html_str.attrs[attr]
    elif hasattr(html_str, 'text'):
        return html_str.text

    return html_str_


def _get_soup(url):
    res = requests.get(url)
    return BeautifulSoup(res.content, 'html.parser')


if __name__ == '__main__':
    argv = sys.argv

    if len(argv) < 2:
        print('Select festival from')
        for festival in festivals.keys():
            print('  {}'.format(festival))
        sys.exit(1)

    festival = argv[1]
    print('Start creating {}\n'.format(festival))

    main(festival)
