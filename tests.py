from unittest import TestCase, main, skip

from bs4 import BeautifulSoup

from fes import Spotify, extract_artist


class MainTest(TestCase):

    def test_extract_artist(self):
        html_str = 'hoge'
        actual = extract_artist(html_str)
        self.assertEqual(actual, 'hoge')

        html_str = '<p>YOGEE NEW WAVES<p></p></p>'
        actual = extract_artist(html_str, pattern=r'</?p>')
        self.assertEqual(actual, 'YOGEE NEW WAVES')

        html_str = '<p>YOGEE NEW WAVES</p>'
        actual = extract_artist(html_str, pattern=r'</?p>')
        self.assertEqual(actual, 'YOGEE NEW WAVES')

        html_str = '<img alt="ATTR_VALUE">YOGEE NEW WAVES</img>'
        html = BeautifulSoup(html_str, 'html.parser')
        actual = extract_artist(html.img, attr='alt')
        self.assertEqual(actual, 'ATTR_VALUE')

    @skip('spotify api key is missing in circleci')
    def test_delete_all_tracks(self):
        sp = Spotify()
        playlist = sp.sp.user_playlist_create(
            sp.user_id, 'hoge', public=False)

        sp.delete_all_tracks(playlist['id'])


if __name__ == '__main__':
    main()
